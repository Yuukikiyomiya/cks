FROM nginx
USER root
COPY ./html /usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf
RUN service nginx start